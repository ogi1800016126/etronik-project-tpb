import 'package:flutter/material.dart';

class TransaksiPage extends StatefulWidget {
  @override
  _TransaksiPageState createState() => _TransaksiPageState();
}

class _TransaksiPageState extends State<TransaksiPage> {
  List names = [
    "MANDIRI",
    "BCA",
    "BNI",
    "BSI",
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Transaksi'),
      ),
      body: Center(
        child: ListView.builder(
          itemBuilder: (BuildContext context, int index) {
            final number = index + 1;
            final name = names[index].toString();
            return Card(
              color: Colors.white,
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    CircleAvatar(
                      backgroundImage: AssetImage('assets/_$number.png'),
                    ),
                    Text(name),
                    Icon(Icons.check_box_outlined),
                  ],
                ),
              ),
            );
          },
          itemCount: names.length,
        ),
      ),
    );
  }
}
