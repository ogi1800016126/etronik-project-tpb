import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AkunPage extends StatefulWidget {
  @override
  _AkunPageState createState() => _AkunPageState();
}

class _AkunPageState extends State<AkunPage> {
  @override
  void initState() {
    super.initState();
  }

  _logOut() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setBool('login', false);
    print('logout');
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/login', (Route<dynamic> route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Profil'),
        ),
        body: SafeArea(
          child: new Container(
              color: Colors.white,
              child: Center(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      CircleAvatar(
                        radius: 150,
                        backgroundImage: AssetImage('assets/ogi.jpg'),
                      ),
                      SizedBox(
                        height: 20.0,
                        width: 300,
                        child: Divider(
                          color: Colors.teal[100],
                        ),
                      ),
                      Text(
                        'Ogi AFdhil Pratama',
                        style: TextStyle(
                          fontSize: 25,
                          color: Colors.black,
                        ),
                      ),
                      Text(
                        '1800016126',
                        style: TextStyle(
                          fontSize: 25,
                          color: Colors.black,
                          letterSpacing: 3.5,
                        ),
                      ),
                      SizedBox(
                        height: 20.0,
                        width: 200,
                        child: Divider(
                          color: Colors.teal[100],
                        ),
                      ),
                      Text(
                        "INFORMASI PENGGUNA",
                        style: TextStyle(color: Colors.black),
                      ),
                      Card(
                          color: Colors.blue[50],
                          margin: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 25.0),
                          child: ListTile(
                            leading: Icon(
                              Icons.phone,
                              color: Colors.blue,
                            ),
                            title: Text(
                              '+683168201651',
                              style: TextStyle(fontSize: 20.0),
                            ),
                          )),
                      Card(
                          color: Colors.blue[50],
                          margin: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 25.0),
                          child: ListTile(
                            leading: Icon(
                              Icons.cake,
                              color: Colors.blue,
                            ),
                            title: Text(
                              '11-10-1999',
                              style: TextStyle(fontSize: 20.0),
                            ),
                          )),
                      RaisedButton(
                        onPressed: () => _logOut(),
                        color: Colors.red,
                        child: const Text('Logout',
                            style: TextStyle(fontSize: 18)),
                      ),
                    ]),
              )),
        ));
  }
}
