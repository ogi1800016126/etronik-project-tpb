# APLIKASI E-TRONIK dengan CODEIGNITER 3

Aplikasi flutter dibangun dengan mengggunakan codeigniter 3 sebagai database. database yang digunakan menggunakana phpmyadmin.dan sudah terdapat file sql nya.ekspor file sqlnya di phpmyadmin.

## [Link File CodeIgniter 3](https://gitlab.com/ogi1800016126/codeigniter-3-dengan-flutter-etronik)

disini saya menggunakana nama file codeignite 3 adalah ci. karna pada file /lib/constans.dart terdapat alamat ip dan nama folder cinya. disini tergantung laptop masing masing yang berbeda alamat ipnya. disini ip pada laptop saya http://192.168.17.217  . dan nama folder ci. . seperti gambar dibawah ini. static String sUrl= "http://192.168.17.217/ci";

![aturkoneksi](/atur koneksi.png)


**Tampilan:**

- Launcher
![launcher](/launcher.png)

- Login
![login](/login.png)

- Login2 , jika password salah. disini username ogipratama dan pass 12 jika berhasil
![login2](/login2.png)

- Beranda
![beranda](/beranda.png)

- Produk detail : jika diklik produk maka muncul produk detail. lalu clik masukan keranjang
![produkdetail](/produkdetail.png)

- Keranjang: disini bisa diatur tambah, kurang, hapus produk dimenu keranjang
![keranjang](/keranjang.png)

- Transaksi
![transaksi](/transaksi.png)

- Profile,  jika sudah login, jika restart aplikasi run makan logout terlebih dahulu, agar kembali kehalaman login
![profil](/profil.png)






## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
